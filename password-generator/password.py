import secrets
import string


def generate_password(length: int = None, sep: str = None, n: int = None) -> str:
    """
    Generates a password of random ascii characters.

    Args:
        length -- Password length, excluding the separator characters.
        sep -- Separator character to insterted after every n:th character. 
               Default is a dash "-".
        n -- The separator character is inserted after every this many 
             characters. Default is 6
    """
    # Default values
    length = 18 if length is None else length
    sep = "-" if sep is None else sep
    n = 6 if n is None else n

    # Alphabet of lower case letters, upper case letters, and the digits 0-9
    alphabet = string.ascii_letters + string.digits

    # Generate the password
    password = ""
    for i in range(length):
        # Append the separator character after every n:th character 
        if i % n == 0 and i > 0:
            password += sep
        # Append a random character from the alphabet
        password += secrets.choice(alphabet)

    return password


if __name__ == "__main__":
    print(generate_password())